#!/bin/bash

# Set variables
directory="$1"
compression="$2"
output_file="$3"
password="notverysecure"
error_log="error.log"

# Redirect all output to the error log file
exec >"$error_log" 2>&1

# Create backup archive, compress it, encrypt it, and write to output file
tar --create --file - --directory "$directory" . | "$compression" | openssl enc -aes256 -pass pass:"$password" -out "$output_file"

# Check if any errors occurred during the backup process
if [ "$?" -ne 0 ]; then
  echo "An error occurred while creating the backup. Check the $error_log file for details."
fi