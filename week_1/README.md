# Backup Script

1. Save the script and make it executable (```chmod +x backup.sh```).
2. Run the script with the three required arguments (directory, compression, output_file):
```./backup.sh /path/to/directory gzip backup.tar.gz.enc```


This will create a backup of the ```/path/to/directory``` directory, compress it using gzip, encrypt it using AES256 encryption with the password ```"notverysecure"```, and save it to the backup.tar.gz.enc file. Any errors that occur during the process will be written to the error.log file.